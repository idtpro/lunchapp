/* tslint:disable */

declare var Object: any;
export interface LuncheventtypeInterface {
  lunchevent_id: number;
  foodtype_id: number;
  id?: number;
}

export class Luncheventtype implements LuncheventtypeInterface {
  lunchevent_id: number;
  foodtype_id: number;
  id: number;
  constructor(instance?: LuncheventtypeInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Luncheventtype`.
   */
  public static getModelName() {
    return "Luncheventtype";
  }
}
