import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { AppRate } from 'ionic-native';
import { Onboarding } from '../onboarding/onboarding';
import { ProfilePage } from '../profile/profile';



@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController) {

  }

  rateApp() {
    AppRate.preferences.storeAppURL = {
      ios: 'ladenzeile.android',
      android: 'market://details?id=ladenzeile.android',
    };

    AppRate.promptForRating(true);
  }

  onboarding() {
    this.navCtrl.push(Onboarding);
  }

  editProfile() {
    this.navCtrl.push(ProfilePage);
  }
}
