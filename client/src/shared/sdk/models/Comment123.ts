/* tslint:disable */
import {
  Vmuser
} from '../index';

declare var Object: any;
export interface Comment123Interface {
  user_id: number;
  commentor_id: number;
  commentText: string;
  created: any;
  id?: number;
  vmuser?: Vmuser;
}

export class Comment123 implements Comment123Interface {
  user_id: number;
  commentor_id: number;
  commentText: string;
  created: any;
  id: number;
  vmuser: Vmuser;
  constructor(instance?: Comment123Interface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Comment123`.
   */
  public static getModelName() {
    return "Comment123";
  }
}
