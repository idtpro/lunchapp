import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

/*
  Generated class for the FiltersPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-filters-page',
  templateUrl: 'filters-page.html'
})
export class FiltersPage {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController) { }

  ionViewDidLoad() {
    console.log('Hello FiltersPage Page');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
