/* tslint:disable */

declare var Object: any;
export interface FoodtypeInterface {
  name: string;
  image_url: string;
  id?: number;
}

export class Foodtype implements FoodtypeInterface {
  name: string;
  image_url: string;
  id: number;
  constructor(instance?: FoodtypeInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Foodtype`.
   */
  public static getModelName() {
    return "Foodtype";
  }
}
