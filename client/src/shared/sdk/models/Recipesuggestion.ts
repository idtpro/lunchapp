/* tslint:disable */

declare var Object: any;
export interface RecipesuggestionInterface {
  name: string;
  image_url?: string;
  description?: string;
  totalcost?: number;
  numberOfGuest?: number;
  id?: number;
}

export class Recipesuggestion implements RecipesuggestionInterface {
  name: string;
  image_url: string;
  description: string;
  totalcost: number;
  numberOfGuest: number;
  id: number;
  constructor(instance?: RecipesuggestionInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Recipesuggestion`.
   */
  public static getModelName() {
    return "Recipesuggestion";
  }
}
