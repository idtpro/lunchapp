/* tslint:disable */
import {
  Vmuser,
  Status,
  Location,
  Luncheventdiet,
  Luncheventguest,
  Timedefinition,
} from '../index';

declare var Object: any;
export interface LuncheventInterface {
  user_id: number;
  image_url?: string;
  billimage?: string;
  name: string;
  description?: string;
  location_id?: number;
  startTime?: number;
  deadTime?: number;
  totalCost?: number;
  numberOfGuest?: number;
  status_id?: number;
  file?: string;
  created?: any;
  lunchdate?: any;
  startTimeCooking?: number;
  id?: number;
  vmuser?: Vmuser;
  status?: Status;
  location?: Location;
  luncheventdiets?: Array<Luncheventdiet>;
  luncheventguests?: Array<Luncheventguest>;
  startTimeDefine?: Timedefinition;
  deadTimeDefine?: Timedefinition;
}

export class Lunchevent implements LuncheventInterface {
  user_id: number;
  image_url: string;
  billimage: string;
  name: string;
  description: string;
  location_id: number;
  startTime: number;
  deadTime: number;
  totalCost: number;
  numberOfGuest: number;
  status_id: number;
  file: string;
  created: any;
  lunchdate: any;
  startTimeCooking: number;
  id: number;
  vmuser: Vmuser;
  status: Status;
  location: Location;
  luncheventdiets: Array<Luncheventdiet>;
  luncheventguests: Array<Luncheventguest>;
  startTimeDefine: Timedefinition;
  deadTimeDefine: Timedefinition;
  constructor(instance?: LuncheventInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Lunchevent`.
   */
  public static getModelName() {
    return "Lunchevent";
  }
}
