/* tslint:disable */

declare var Object: any;
export interface ContainerInterface {
  id?: number;
}

export class Container implements ContainerInterface {
  id: number;
  constructor(instance?: ContainerInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Container`.
   */
  public static getModelName() {
    return "Container";
  }
}
