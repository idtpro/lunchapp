/* tslint:disable */

declare var Object: any;
export interface LuncheventguestrateInterface {
  rate_id: number;
  user_id: number;
  rateValue: number;
  id?: number;
}

export class Luncheventguestrate implements LuncheventguestrateInterface {
  rate_id: number;
  user_id: number;
  rateValue: number;
  id: number;
  constructor(instance?: LuncheventguestrateInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Luncheventguestrate`.
   */
  public static getModelName() {
    return "Luncheventguestrate";
  }
}
