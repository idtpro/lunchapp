/* tslint:disable */

declare var Object: any;
export interface UsersuggestiontypeInterface {
  usersuggestion_id: number;
  foodtype_id: number;
  id?: number;
}

export class Usersuggestiontype implements UsersuggestiontypeInterface {
  usersuggestion_id: number;
  foodtype_id: number;
  id: number;
  constructor(instance?: UsersuggestiontypeInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Usersuggestiontype`.
   */
  public static getModelName() {
    return "Usersuggestiontype";
  }
}
