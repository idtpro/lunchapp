import { Component } from '@angular/core';

import { NavController, Tabs } from 'ionic-angular';
import { Lunchevent, LuncheventApi, Location, LocationApi, Timedefinition, TimedefinitionApi, Diet, DietApi, LuncheventdietApi } from '../../shared';
import { ToastController } from 'ionic-angular';
import { Camera } from 'ionic-native';
import { LoopBackAuth, Luncheventdiet } from '../../shared';


@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  public lunchevnet: Lunchevent;
  public locations: Location[];
  public timeDefinitions: Timedefinition[];
  public diets: Diet[][];
  public src: string = "assets/dumy-food.png";

  constructor(public navCtrl: NavController, public luncheventApi: LuncheventApi, public toastCtrl: ToastController, public nav: NavController
    , public loopBackAuth: LoopBackAuth, public locationApi: LocationApi, public timeDefinitionApi: TimedefinitionApi, public dietApi: DietApi,
    public luncheventdietApi: LuncheventdietApi) {
    this.locationApi.find().subscribe(data => {
      this.locations = data;
    }, err => err);

    this.timeDefinitionApi.find().subscribe(data => {
      this.timeDefinitions = data;
    }, err => err);
    this.dietApi.find().subscribe(data => {
      this.diets = [];
      for (var i = 0; i < data.length; i++) {
        let row = Math.floor(i / 3);
        if (!this.diets[row]) this.diets[row] = [];
        this.diets[row][i % 3] = data[i];
      }
    }, err => err);
  }

  ionViewDidLoad() {
    this.lunchevnet = new Lunchevent();
    this.lunchevnet.luncheventdiets = [];
  }

  create() {
    this.lunchevnet.user_id = this.loopBackAuth.getCurrentUserId();
    this.lunchevnet.created = new Date();
    this.luncheventApi.create(this.lunchevnet).subscribe(data => {
      this.showToast('Created successfully');
      this.lunchevnet.luncheventdiets.forEach(l => {
        l.lunchevent_id = data.id;
        this.luncheventdietApi.create(l).subscribe(data => data = data, err => console.log(err));
        var t: Tabs = this.nav.parent;
        t.select(0);
      });
    }, err => {
      this.showToast('err  ' + err);
    });

  }

  showToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  takePicture() {
    let options = {
      "sourceType": Camera.PictureSourceType.PHOTOLIBRARY, "destinationType": Camera.DestinationType.DATA_URL,
      "quality": 35, "targetWidth": 600, "targetHeight": 600, "correctOrientation": true
    };
    Camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      //      var the_file = this.b64toBlob(imageData, "image/png", 512);
      //console.log(imageData);

      this.lunchevnet.file = 'data:image/jpeg;base64,' + imageData;
      this.src = this.lunchevnet.file;
      //let req = { "file": the_file, "container": "img" };
      //this.containerApi.upload(req)
    }, (err) => {
      // Handle error
    });
  }

  getPricePerPerson() {
    if (!this.lunchevnet.totalCost || !this.lunchevnet.numberOfGuest) {
      return "";
    }
    return Math.round(this.lunchevnet.totalCost * 10 / this.lunchevnet.numberOfGuest) / 10 + "";
  }

  addDiet(diet: Diet) {
    if (this.checkDiet(diet)) {
      this.lunchevnet.luncheventdiets = this.lunchevnet.luncheventdiets.filter(ld => ld.diet_id != diet.id);
    }
    else {
      let luncheventdiet = new Luncheventdiet();
      luncheventdiet.lunchevent_id = this.lunchevnet.id;
      luncheventdiet.diet_id = diet.id;
      //diet.selected = true;
      this.lunchevnet.luncheventdiets.push(luncheventdiet);

    }
  }

  checkDiet(diet: Diet) {
    var arr = this.lunchevnet.luncheventdiets.filter(ld => ld.diet_id == diet.id);
    return arr.length > 0;
  }
}
