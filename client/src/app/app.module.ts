import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { TabsPage } from '../pages/tabs/tabs';
import { EventDetailPage } from '../pages/event-detail-page/event-detail-page';
import { RatingPage } from '../pages/rating-page/rating-page';
import { FiltersPage } from '../pages/filters-page/filters-page';
import { Login } from '../pages/login/login';
import { Onboarding } from '../pages/onboarding/onboarding';
import { SDKModule } from '../shared/sdk';
import { Stars } from '../providers/star-pipe';


@NgModule({
  declarations: [
    MyApp,
    Login,
    AboutPage,
    ContactPage,
    HomePage,
    ProfilePage,
    EventDetailPage,
    RatingPage,
    FiltersPage,
    TabsPage,
    Onboarding,
    Stars
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    SDKModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Login,
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    ProfilePage,
    EventDetailPage,
    RatingPage,
    FiltersPage,
    Onboarding,
    TabsPage
  ],
  providers: [Stars]
})
export class AppModule { }
