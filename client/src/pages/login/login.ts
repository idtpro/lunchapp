import { Component } from '@angular/core';
import { UserService } from '../../providers/user-service';
import { Vmuser } from '../../shared';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [UserService]
})
export class Login {
  public vmuser: Vmuser;

  constructor(public userService: UserService) {
    this.vmuser = new Vmuser();
  }

  ionViewDidLoad() {
    console.log('Hello Login Page');
  }

  login() {
    this.userService.login(this.vmuser);
  }
}
