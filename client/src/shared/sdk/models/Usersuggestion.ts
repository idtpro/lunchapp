/* tslint:disable */

declare var Object: any;
export interface UsersuggestionInterface {
  suggestion: string;
  id?: number;
}

export class Usersuggestion implements UsersuggestionInterface {
  suggestion: string;
  id: number;
  constructor(instance?: UsersuggestionInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Usersuggestion`.
   */
  public static getModelName() {
    return "Usersuggestion";
  }
}
