import { Pipe } from '@angular/core';
import { Vmuser } from '../shared';

@Pipe({ name: 'stars' })
export class Stars {
    transform(value: Vmuser): any {
        let arr = []
        let starNumber = Math.floor(value.rating / 20);
        for (let i = 0; i < starNumber; i++) {
            arr.push("star");
        }
        if ((value.rating % 20) >= 10) {
            arr.push("star-half");
            starNumber++;
        }
        for (let i = starNumber; i < 5; i++) {
            arr.push("star-outline");
        }
        return arr;
    }
}
