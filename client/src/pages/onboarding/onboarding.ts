import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the Onboarding page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-onboarding',
  templateUrl: 'onboarding.html'
})
export class Onboarding {
  public slides: string[] = [];
  public mySlideOptions = {
    pager: true
  }
  constructor(public navCtrl: NavController) {
    // this.slides.push('onboarding-ani2.gif');
    this.slides.push('slide-text-1.png');
    this.slides.push('slide-text-2.png');
    this.slides.push('slide-text-3.png');
    this.slides.push('slide-text-4.png');

  }

  ionViewDidLoad() {
    console.log('Hello Onboarding Page');
  }

}
