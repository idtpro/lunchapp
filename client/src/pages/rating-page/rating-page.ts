import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Rating, RatingApi } from '../../shared';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';

/*
  Generated class for the RatingPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-rating-page',
  templateUrl: 'rating-page.html'
})
export class RatingPage {
  @ViewChild('mySlider') slider: Slides;
  public ratings: Rating[];
  public mySlideOptions = {
    pager: true
  }


  constructor(public navCtrl: NavController, ratingApi: RatingApi) {
    ratingApi.find().subscribe(data => this.ratings = data);

  }

  ionViewDidLoad() {
    console.log('Hello RatingPage Page');
  }

  next(rating, value) {
    this.slider.slideNext();
  }

  back() {
    this.navCtrl.pop();
  }
}
