/* tslint:disable */

declare var Object: any;
export interface UserfeedbackInterface {
  user_id: number;
  feedback_user_id: number;
  feedback: string;
  id?: number;
}

export class Userfeedback implements UserfeedbackInterface {
  user_id: number;
  feedback_user_id: number;
  feedback: string;
  id: number;
  constructor(instance?: UserfeedbackInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Userfeedback`.
   */
  public static getModelName() {
    return "Userfeedback";
  }
}
