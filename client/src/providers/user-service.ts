import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { LoopBackAuth, VmuserApi, Vmuser, AccessToken } from '../shared';
import 'rxjs/add/operator/map';
import { NativeStorage } from 'ionic-native';
import { ToastController } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { TabsPage } from '../pages/tabs/tabs';


/*
  Generated class for the UserService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UserService {
  public vmuser: Vmuser;
  public accessToken: AccessToken;

  constructor(public http: Http, private vmuserApi: VmuserApi, public toastCtrl: ToastController, public nav: NavController, public loopBackAuth: LoopBackAuth) {
  }
  init() {
    if (1 == 1)
      return;
    this.vmuser = new Vmuser();
    this.vmuser.username = "hadi";
    this.vmuser.password = "123456";
    if (this.loopBackAuth.getCurrentUserId() == null) {
      this.login(this.vmuser);
      try {
        NativeStorage.getItem('user')
          .then(
          data => {
            if (data) {
              this.vmuser = JSON.parse(data);
              this.login(this.vmuser);
            }
          },
          error => console.error(error)
          );

      } catch (error) {

      }

    }
  }

  saveUser() {
    try {
      NativeStorage.setItem('user', JSON.stringify(this.vmuser))
        .then(
        () => console.log('Stored item!'),
        error => console.error('Error storing item', error)
        );

    } catch (error) {

    }
  }

  login(vmuser: Vmuser) {
    this.vmuserApi.login({ "username": vmuser.username, "password": vmuser.password }).subscribe((token: AccessToken) => {
      this.vmuser = vmuser;
      this.accessToken = token;
      this.vmuser.id = parseInt(token.userId);
      this.saveUser();
      this.nav.setRoot(TabsPage);
    }, err => {

      if (err.statusCode == 401 && this.loopBackAuth.getCurrentUserId() == null) {
        this.presentToast();
      }
    }
    );


  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'User or password error',
      duration: 3000
    });
    toast.present();
  }
}
