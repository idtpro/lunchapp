module.exports = {
    include: [
        {
            src: 'src/assets/',
            dest: 'www/assets/'
        },
        {
            src: 'src/index.html',
            dest: 'www/index.html'
        },
        {
            src: 'node_modules/ionic-angular/polyfills/polyfills.js',
            dest: 'www/build/polyfills.js'
        },
        {
            src: 'node_modules/ionicons/dist/fonts/',
            dest: 'www/assets/fonts/'
        },
        // google-services files
        {
            src: 'google-services.json',
            dest: 'platforms/android/google-services.json'
        },
        {
            src: 'GoogleService-Info.plist',
            dest: 'platforms/ios/GoogleService-Info.plist'
        },
    ]
};