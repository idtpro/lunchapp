/* tslint:disable */

declare var Object: any;
export interface UserfilterInterface {
  user_id: number;
  filters: string;
  id?: number;
}

export class Userfilter implements UserfilterInterface {
  user_id: number;
  filters: string;
  id: number;
  constructor(instance?: UserfilterInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Userfilter`.
   */
  public static getModelName() {
    return "Userfilter";
  }
}
