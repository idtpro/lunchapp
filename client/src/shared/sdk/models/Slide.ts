/* tslint:disable */

declare var Object: any;
export interface SlideInterface {
  name: string;
  id?: number;
}

export class Slide implements SlideInterface {
  name: string;
  id: number;
  constructor(instance?: SlideInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Slide`.
   */
  public static getModelName() {
    return "Slide";
  }
}
