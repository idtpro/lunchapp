/* tslint:disable */

declare var Object: any;
export interface LuncheventmediaInterface {
  media_url: string;
  id?: number;
}

export class Luncheventmedia implements LuncheventmediaInterface {
  media_url: string;
  id: number;
  constructor(instance?: LuncheventmediaInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Luncheventmedia`.
   */
  public static getModelName() {
    return "Luncheventmedia";
  }
}
