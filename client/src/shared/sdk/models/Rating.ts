/* tslint:disable */

declare var Object: any;
export interface RatingInterface {
  question: string;
  id?: number;
}

export class Rating implements RatingInterface {
  question: string;
  id: number;
  constructor(instance?: RatingInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Rating`.
   */
  public static getModelName() {
    return "Rating";
  }
}
