var fs = require('fs');

module.exports = function (app) {
    var alreadyDone = [];
    var mysqlDs = app.dataSources.mysqlDs;
    for (var k in app.models) {
        var mdl = app.models[k];
        if (mdl && mdl.dataSource && mdl.dataSource.name == "mysql") {
            console.log(k);
            mysqlDs.automigrate(k, function (err) {
            });

        }
    }
    setTimeout(function () {
        fs.readdir('data', (err, files) => {
            files.forEach(file => {
                readFile(file);
                console.log(file);
            });
        });

    }, 1000);

    /*    var Vmuser = app.models.Vmuser;
        Vmuser.create({
            "image_url": "string",
            "dep_id": 0,
            "nickname": "hadi",
            "likeToEat": "string",
            "likeToCook": "string",
            "pushNotification": true,
            "realm": "string",
            "username": "hadikashlan",
            "email": "hkashlan@gmail.com",
            "emailVerified": true,
            "password": "123456123"
        });
    */

    function readFile(file) {
        file = file.replace(".csv", "");
        var model = app.models[file];
        try {
            var path = 'data/' + file.toLowerCase() + '.csv';
            var lineReader = require('readline').createInterface({
                input: require('fs').createReadStream(path)
            });
            var firstLine;

            lineReader.on('line', function (line) {
                try {
                    if (!lineReader.firstLine) {
                        lineReader.firstLine = line.split(',');
                    }
                    else {
                        var values = line.split(',');
                        var rec = {};
                        for (var k in values) {
                            rec[lineReader.firstLine[k]] = values[k];
                        }
                        try {

                            model.create(rec);
                        } catch (error) {
                            console.log(error);
                            console.log(rec);
                        }
                        //                       console.log(rec);
                    }

                } catch (error) {
                    // console.log(error);
                }
            });

        } catch (error) {
            // console.log(error);
        }
    }
}