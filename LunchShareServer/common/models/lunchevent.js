'use strict';

module.exports = function (Lunchevent) {

    //     MyModel.observe('before save', function updateTimestamp(ctx, next) {
    //   if (ctx.instance) {
    //     ctx.instance.updated = new Date();
    //   } else {
    //     ctx.data.updated = new Date();
    //   }
    //   next();
    // });
    Lunchevent.beforeSave = function (next, modelInstance) {
        var fs = require('fs');
        var uuid = require('node-uuid');
        var fileName = uuid.v1() + ".png";
        var filePath = "client/img/food/" + fileName;
        if (modelInstance.file) {
            var imageBuffer = decodeBase64Image(modelInstance.file);
            fs.writeFile(filePath, imageBuffer.data, function (err) {
                if (err) {
                    return console.log(err);
                }

                console.log("The file was saved!");
                delete modelInstance.file;
                modelInstance.file = null;
                modelInstance.image_url = fileName;
                modelInstance.created = new Date();

                next();
            });
        }
        else {

            next();
        }
        //your logic goes here
    };

    Lunchevent.simpleUpload = function (file, cb) {
        console.log(file);
        file = file.file;
        console.log(file);
        if (1 == 1) return cb(null, "sdf");
        var fs = require('fs');
        var uuid = require('node-uuid');
        var filePath = "client/img/" + uuid.v1() + ".png";
        var imageBuffer = decodeBase64Image(file);
        fs.writeFile(filePath, imageBuffer.data, function (err) {
            if (err) {
                return console.log(err);
            }

            console.log("The file was saved!");
            cb(null, filePath);
        });
    }

    Lunchevent.remoteMethod(
        'simpleUpload',
        {
            accepts: { arg: 'file', type: 'object', http: { source: 'body' } },
            returns: { arg: 'uploaded', type: 'string' }
        }

    );

    function decodeBase64Image(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
            response = {};

        if (matches.length !== 3) {
            return new Error('Invalid input string');
        }

        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');

        return response;
    }
};
