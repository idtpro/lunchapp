/* tslint:disable */

declare var Object: any;
export interface LocationInterface {
  name: string;
  id?: number;
}

export class Location implements LocationInterface {
  name: string;
  id: number;
  constructor(instance?: LocationInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Location`.
   */
  public static getModelName() {
    return "Location";
  }
}
