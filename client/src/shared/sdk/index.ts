/**
* @module SDKModule
* @author Jonathan Casarrubias <t:@johncasarrubias> <gh:jonathan-casarrubias>
* @license MTI 2016 Jonathan Casarrubias
* @description
* The SDKModule is a generated Software Development Kit automatically built by
* the LoopBack SDK Builder open source module.
*
* The SDKModule provides Angular 2 >= RC.5 support, which means that NgModules
* can import this Software Development Kit as follows:
*
*
* APP Route Module Context
* ============================================================================
* import { NgModule }       from '@angular/core';
* import { BrowserModule }  from '@angular/platform-browser';
* // App Root 
* import { AppComponent }   from './app.component';
* // Feature Modules
* import { SDKModule }      from './shared/sdk/sdk.module';
* // Import Routing
* import { routing }        from './app.routing';
* @NgModule({
*  imports: [
*    BrowserModule,
*    routing,
*    SDKModule.forRoot()
*  ],
*  declarations: [ AppComponent ],
*  bootstrap:    [ AppComponent ]
* })
* export class AppModule { }
*
**/
import { JSONSearchParams } from './services/core/search.params';
import { ErrorHandler } from './services/core/error.service';
import { LoopBackAuth } from './services/core/auth.service';
import { LoggerService } from './services/custom/logger.service';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { UserApi } from './services/custom/User';
import { SlideApi } from './services/custom/Slide';
import { StatusApi } from './services/custom/Status';
import { RatingApi } from './services/custom/Rating';
import { DietApi } from './services/custom/Diet';
import { FoodtypeApi } from './services/custom/Foodtype';
import { LuncheventguestrateApi } from './services/custom/Luncheventguestrate';
import { Comment123Api } from './services/custom/Comment123';
import { LocationApi } from './services/custom/Location';
import { VmuserApi } from './services/custom/Vmuser';
import { RecipesuggestionApi } from './services/custom/Recipesuggestion';
import { UsersuggestionApi } from './services/custom/Usersuggestion';
import { UsersuggestiontypeApi } from './services/custom/Usersuggestiontype';
import { UsersugeestiondietApi } from './services/custom/Usersugeestiondiet';
import { UserfeedbackApi } from './services/custom/Userfeedback';
import { UserfilterApi } from './services/custom/Userfilter';
import { LuncheventApi } from './services/custom/Lunchevent';
import { LuncheventmediaApi } from './services/custom/Luncheventmedia';
import { LuncheventtypeApi } from './services/custom/Luncheventtype';
import { LuncheventdietApi } from './services/custom/Luncheventdiet';
import { LuncheventguestApi } from './services/custom/Luncheventguest';
import { ContainerApi } from './services/custom/Container';
import { DepartmentApi } from './services/custom/Department';
import { TimedefinitionApi } from './services/custom/Timedefinition';

@NgModule({
  imports: [CommonModule, HttpModule],
  declarations: [],
  exports: [],
  providers: []
})

export class SDKModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SDKModule,
      providers: [
        LoopBackAuth,
        ErrorHandler,
        LoggerService,
        JSONSearchParams,
        UserApi,
        SlideApi,
        StatusApi,
        RatingApi,
        DietApi,
        FoodtypeApi,
        LuncheventguestrateApi,
        Comment123Api,
        LocationApi,
        VmuserApi,
        RecipesuggestionApi,
        UsersuggestionApi,
        UsersuggestiontypeApi,
        UsersugeestiondietApi,
        UserfeedbackApi,
        UserfilterApi,
        LuncheventApi,
        LuncheventmediaApi,
        LuncheventtypeApi,
        LuncheventdietApi,
        LuncheventguestApi,
        ContainerApi,
        DepartmentApi,
        TimedefinitionApi
      ]
    };
  }
}

export * from './models/index';
export * from './services/index';
export * from './lb.config';
