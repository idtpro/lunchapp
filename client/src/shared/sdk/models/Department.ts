/* tslint:disable */

declare var Object: any;
export interface DepartmentInterface {
  name: string;
  id?: number;
}

export class Department implements DepartmentInterface {
  name: string;
  id: number;
  constructor(instance?: DepartmentInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Department`.
   */
  public static getModelName() {
    return "Department";
  }
}
