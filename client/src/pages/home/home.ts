import { Component } from '@angular/core';

import { NavController, ModalController } from 'ionic-angular';
import { LoopBackAuth, LuncheventApi, Lunchevent, Luncheventguest, LuncheventguestApi } from '../../shared';
import { FiltersPage } from '../filters-page/filters-page';
import { EventDetailPage } from '../event-detail-page/event-detail-page';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  public lunchevents: Lunchevent[];

  constructor(public navCtrl: NavController, public loopBackAuth: LoopBackAuth,
    public luncheventApi: LuncheventApi, public modalController: ModalController,
    public luncheventguestApi: LuncheventguestApi) {
  }

  ionViewWillEnter() {
    this.luncheventApi.find({ "order": "created desc", "include": ["luncheventdiets", "startTimeCooking", "vmuser", "status", "location", "deadTimeDefine", "startTimeDefine", "luncheventguests"] }).subscribe(lunchevents => {
      this.lunchevents = lunchevents
    });
    let tt = this.loopBackAuth.getAccessTokenId();
    if (tt == null) {
      console.log("correct");
    }
    else {
      console.log("tt no correct" + tt);

    }

  }
  checkThat() {
    this.lunchevents = this.lunchevents;
  }

  join(lunchevent: Lunchevent) {
    let luncheventGuest: Luncheventguest = new Luncheventguest();
    luncheventGuest.bookTime = new Date();
    luncheventGuest.lunchevent_id = lunchevent.id;
    luncheventGuest.user_id = this.loopBackAuth.getCurrentUserId();
    this.luncheventguestApi.create(luncheventGuest).subscribe(data => {
      lunchevent.luncheventguests.push(data);
    }, err => console.log(err));
  }

  openFilter() {
    let dlg = this.modalController.create(FiltersPage);
    dlg.present();
  }

  checkLunchItem(lunchevent: Lunchevent) {
    let temp = lunchevent.luncheventguests.filter(luncheventguest => luncheventguest.user_id == this.loopBackAuth.getCurrentUserId());
    return temp.length != 0;
  }


  goEvetDetail(lunchevent: Lunchevent) {
    this.navCtrl.push(EventDetailPage, { lunchevent: lunchevent });
  }
}
