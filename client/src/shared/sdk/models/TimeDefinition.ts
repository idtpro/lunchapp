/* tslint:disable */

declare var Object: any;
export interface TimedefinitionInterface {
  name: string;
  id?: number;
}

export class Timedefinition implements TimedefinitionInterface {
  name: string;
  id: number;
  constructor(instance?: TimedefinitionInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Timedefinition`.
   */
  public static getModelName() {
    return "Timedefinition";
  }
}
