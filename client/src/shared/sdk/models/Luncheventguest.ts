/* tslint:disable */

declare var Object: any;
export interface LuncheventguestInterface {
  lunchevent_id: number;
  user_id: number;
  bookTime?: any;
  status?: number;
  id?: number;
}

export class Luncheventguest implements LuncheventguestInterface {
  lunchevent_id: number;
  user_id: number;
  bookTime: any;
  status: number;
  id: number;
  constructor(instance?: LuncheventguestInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Luncheventguest`.
   */
  public static getModelName() {
    return "Luncheventguest";
  }
}
