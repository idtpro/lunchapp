import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { Lunchevent } from '../../shared';
import { RatingPage } from '../rating-page/rating-page';


/*
  Generated class for the EventDetailPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-event-detail-page',
  templateUrl: 'event-detail-page.html'
})
export class EventDetailPage {
  public lunchevent: Lunchevent;

  constructor(public navCtrl: NavController, private navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('Hello EventDetailPage Page');
    this.lunchevent = this.navParams.get('lunchevent');
  }

  rating() {
    this.navCtrl.push(RatingPage);
  }
}
