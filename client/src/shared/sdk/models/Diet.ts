/* tslint:disable */

declare var Object: any;
export interface DietInterface {
  name: string;
  image_url: string;
  id?: number;
}

export class Diet implements DietInterface {
  name: string;
  image_url: string;
  id: number;
  constructor(instance?: DietInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Diet`.
   */
  public static getModelName() {
    return "Diet";
  }
}
