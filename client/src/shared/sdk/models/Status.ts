/* tslint:disable */

declare var Object: any;
export interface StatusInterface {
  name: string;
  fireRating?: boolean;
  id?: number;
}

export class Status implements StatusInterface {
  name: string;
  fireRating: boolean;
  id: number;
  constructor(instance?: StatusInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Status`.
   */
  public static getModelName() {
    return "Status";
  }
}
