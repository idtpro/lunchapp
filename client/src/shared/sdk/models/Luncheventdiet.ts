/* tslint:disable */

declare var Object: any;
export interface LuncheventdietInterface {
  lunchevent_id: number;
  diet_id: number;
  id?: number;
}

export class Luncheventdiet implements LuncheventdietInterface {
  lunchevent_id: number;
  diet_id: number;
  id: number;
  constructor(instance?: LuncheventdietInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Luncheventdiet`.
   */
  public static getModelName() {
    return "Luncheventdiet";
  }
}
