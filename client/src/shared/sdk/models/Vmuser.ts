/* tslint:disable */

declare var Object: any;
export interface VmuserInterface {
  image_url?: string;
  dep_id?: number;
  nickname?: string;
  likeToEat?: string;
  likeToCook?: string;
  pushNotification?: boolean;
  rating?: number;
  realm?: string;
  username?: string;
  password: string;
  email: string;
  emailVerified?: boolean;
  verificationToken?: string;
  id?: number;
  accessTokens?: Array<any>;
}

export class Vmuser implements VmuserInterface {
  image_url: string;
  dep_id: number;
  nickname: string;
  likeToEat: string;
  likeToCook: string;
  pushNotification: boolean;
  rating: number;
  realm: string;
  username: string;
  password: string;
  email: string;
  emailVerified: boolean;
  verificationToken: string;
  id: number;
  accessTokens: Array<any>;
  constructor(instance?: VmuserInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Vmuser`.
   */
  public static getModelName() {
    return "Vmuser";
  }
}
