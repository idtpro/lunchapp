import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { LoopBackAuth, Vmuser, Comment123, Comment123Api } from '../../shared';

/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  public vmuser: Vmuser;
  public comments: Comment123[];

  constructor(public navCtrl: NavController, private navParams: NavParams, public loopBackAuth: LoopBackAuth
    , public commentApi: Comment123Api) { }

  ionViewDidLoad() {
    console.log('Hello Profile Page');
    this.vmuser = this.loopBackAuth.getCurrentUserData();
    this.commentApi.find({ "include": ["vmuser"] }).subscribe(data => this.comments = data);
  }

}
