/* tslint:disable */

declare var Object: any;
export interface UsersugeestiondietInterface {
  usersuggestion_id: number;
  diet_id: number;
  id?: number;
}

export class Usersugeestiondiet implements UsersugeestiondietInterface {
  usersuggestion_id: number;
  diet_id: number;
  id: number;
  constructor(instance?: UsersugeestiondietInterface) {
    Object.assign(this, instance);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Usersugeestiondiet`.
   */
  public static getModelName() {
    return "Usersugeestiondiet";
  }
}
